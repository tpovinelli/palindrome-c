#include "stack.h"
#include <ctype.h>
#include <stdbool.h>
#include <unistd.h>

int returnValue = 0;

void usage() {
  fprintf(stderr, "usage: ./palindrome [ -i ] [ -s ] TEST_WORD\n");
}

void terminate() {
  fprintf(stderr, "terminate() called! Exiting...\n");
#ifdef ABRT_ON_TERM
  abort();
#else
  exit(-1);
#endif
}

struct arguments {
  bool ignore_space;
  bool ignore_case;
  char *word;
};

void arguments_init(struct arguments *a) {
  a->ignore_case = false;
  a->ignore_space = false;
  a->word = NULL;
}

void arguments_fini(struct arguments *a) { free(a->word); }

void get_args(int argc, char *const argv[], struct arguments *output) {
  for (;;) {
    switch (getopt(argc, argv, "isw:")) {
    case 'i':
      output->ignore_case = true;
      continue;
    case 's':
      output->ignore_space = true;
      continue;
    case 'w':
      output->word = strdup(optarg);
      continue;
    case 'h':
      usage();
      exit(1);
    case -1:
      goto args_done;
    case '?':
      usage();
      terminate();
    }
  }
args_done:
  if (output->word == NULL) {
    fprintf(stderr, "supply an arugment using -w to test\n");
    terminate();
  }
}

char *despace(char *c) {
  size_t len = 0;
  size_t spc = 0;
  char *w = c;
  while (*w) {
    len++;
    spc += (isspace(*w) == 0 ? 0 : 1);
    w++;
  }
  char *n = malloc((len - spc + 1) * sizeof(char));

  for (size_t i = 0; i < len; i++) {
    if (!isspace(c[i])) {
      n[i] = c[i];
    }
  }
  free(c);
  return n;
}

int main(int argc, char *const argv[]) {
  struct arguments args;
  arguments_init(&args);
  get_args(argc, argv, &args);

  struct stack s;
  stack_init(&s);
  if (args.ignore_space) {
    args.word = despace(args.word);
  }
  char *c = args.word; 
  size_t len = strlen(c);
  size_t index = 0;
  for (; index < len / 2; index++) {
    stack_push(&s, args.ignore_case ? tolower(c[index]) : c[index]);
  }
  index = index + (len % 2);
  for (; index < len; index++) {
    if (stack_pop(&s) != (args.ignore_case ? tolower(c[index]) : c[index])) {
      printf("false\n");
      returnValue = 1;
      goto done;
    }
  }
  printf("true\n");

done:
  stack_free(&s);
  arguments_fini(&args);
  return returnValue;
}
