#ifndef STACK_H
#define STACK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
  int c;
  struct node *next;
  struct node *prev;
};

struct stack {
  struct node *top;
  size_t len;
};

void stack_init(struct stack *s);

void stack_push(struct stack *s, int v);

int stack_pop(struct stack *s);

int stack_peek(struct stack *s);

int *stack_top(struct stack *s);

void stack_set(struct stack *s, int v);

int stack_swap(struct stack *s, int n);

void stack_free(struct stack *s);

#endif
