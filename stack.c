#include "stack.h"

#define REQ_NOT_NULL(val, message)                                             \
  if (val == NULL) {                                                           \
    fprintf(stderr, message "\n");                                             \
    abort();                                                                   \
  }

#define REQ_NOT_STACK_EMPTY(stack, method)                                     \
  REQ_NOT_NULL((stack)->top, method "() from empty stack")

void stack_init(struct stack *s) {
  s->top = NULL;
  s->len = 0;
}

void stack_push(struct stack *s, int v) {
  struct node *n = malloc(sizeof(struct node));
  REQ_NOT_NULL(n, "push() failed to allocate memory");
  n->c = v;
  n->next = NULL;
  if (s->top == NULL) {
    n->prev = NULL;
  } else {
    n->prev = s->top;
    s->top->next = n;
  }
  s->top = n;
  s->len++;
}

int stack_pop(struct stack *s) {
  REQ_NOT_STACK_EMPTY(s, "pop");
  int val = s->top->c;
  struct node *n = s->top;
  s->top = s->top->prev;
  s->len--;
  free(n);
  return val;
}

int stack_peek(struct stack *s) {
  REQ_NOT_STACK_EMPTY(s, "peek");
  return s->top->c;
}

int *stack_top(struct stack *s) {
  REQ_NOT_STACK_EMPTY(s, "top");
  return &(s->top->c);
}

void stack_set(struct stack *s, int v) {
  REQ_NOT_STACK_EMPTY(s, "set");
  *stack_top(s) = v;
}

int stack_swap(struct stack *s, int n) {
  REQ_NOT_STACK_EMPTY(s, "swap");
  int old = stack_peek(s);
  stack_set(s, n);
  return old;
}

void stack_free(struct stack *s) {
  while (s->top != NULL) {
    struct node *n = s->top;
    s->top = s->top->prev;
    s->len--;
    free(n);
  }
}
