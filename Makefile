
INCLUDES = -I/Users/thomaspovinelli/.local/include -I.
OUTFILE = palindrome
WARNINGS = -Wall -Wextra 
FILES = main.c stack.c

all: clean main.c
	clang $(INCLUDES) -o $(OUTFILE) -O3 $(WARNINGS) $(FILES)

debug: main.c
	clang $(INCLUDES) -o $(OUTFILE) -O0 -g $(WARNINGS) $(FILES)

clean: main.c 
	rm -r *.dSYM || true
